export class Article {
    idLogement: number;
    id: number;
    nom : string;
    titre : string;
    prix : string;
    description : string;
    categorie : string;
    image : string;
    pays : string;
}
