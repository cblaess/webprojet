<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

date_default_timezone_set('America/Lima');

require_once "vendor/autoload.php";


$isDevMode = true;
$config = Setup::createYAMLMetadataConfiguration(array(__DIR__ . "/config/yaml"), $isDevMode);
$conn = array(
'driver' => 'pdo_mysql',
'host' => 'localhost',
'user' => 'root',
'password' => 'rootpassword',
'dbname' => 'projetWeb',
'port' => '3306'
);
/*
$conn = array(
    'driver' => 'pdo_mysql',
    'host' => 'projetangular.ccyf6pbxos1j.us-east-1.rds.amazonaws.com',
    'user' => 'admin',
    'password' => 'Azerty123456',
    'dbname' => 'ProjetWeb',
    'port' => '3306'
    );
*/

$entityManager = EntityManager::create($conn, $config);