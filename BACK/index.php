<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;
use Doctrine\ORM\EntityManager;

require 'vendor/autoload.php';
require_once 'bootstrap.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Authorization');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

$app = new \Slim\App();
$app->logins = array("corentin" => "blaess", "leo" => "shz");

const JWT_SECRET = "mykey123456789";
$jwt = new \Tuupola\Middleware\JwtAuthentication([
    "path" => "/api",
    "secure" => false,
    "ignore" => ["/api/users/login","/api/users/register"],
    "secret" => JWT_SECRET,
    "attribute" => "decoded_token_data",
    "algorithm" => ["HS256"],
    "error" => function ($response, $arguments) {
        $data = array('ERREUR' => 'ERREUR', 'ERREUR' => 'AUTO');
        return $response->withHeader("Content-Type", "application/json")->getBody()->write(json_encode($data));
    }
]);

$app->add($jwt);

// retourne le logement avec l'id
$app->get('/api/logements/{id}', function (Request $request, Response $response, array $args){
    global $entityManager;

    $logement = $entityManager->getRepository('Logements')->find($args['id']);

    $res = array(
        'idLogement' => $logement->getId(),
        'titre' => $logement->getTitre(),
        'prix' => $logement->getPrix(),
        'description' => $logement->getDescription(),
        'categorie' => $logement->getCategorie(),
        'image' => $logement->getImage(),
        'pays' => $logement->getPays()
    );

    $obj = json_encode($res);
    $response->write($obj);
    return $response;
});

// retourne la liste de tous les logements
$app->get('/api/logements', function (Request $request, Response $response, array $args){
    global $entityManager;

    $logementsRepository = $entityManager->getRepository('Logements');
    $logements = $logementsRepository->findAll();

    $res = array();
    foreach($logements as $logement)
    {
        $res[] = array(
            'idLogement' => $logement->getId(),
            'titre' => $logement->getTitre(),
            'prix' => $logement->getPrix(),
            'description' => $logement->getDescription(),
            'categorie' => $logement->getCategorie(),
            'image' => $logement->getImage(),
            'pays' => $logement->getPays()
        );
    }
    return $response->write(json_encode($res));
});


// retourne si le login - mdp est valide ou non
$app->post('/api/users/login', function (Request $request, Response $response, array $args) use ($app){
        $body = $request->getParsedBody(); // Parse le body
        $searchLogin = $body['login'];
        $searchPwd = $body['password']; 
    
        global $entityManager;
        $user = $entityManager->getRepository('Clients')->findOneByLogin($searchLogin);
    
        if($user != null)
        {
            $res = array(
                'login' => $user->getLogin(),
                'mdp' => $user->getMdp(),
            );

            if(sha1($searchPwd) == $user->getMdp())
            {
                $logged = true;
            }
        }
        
        // le login et mdp est correct
        if($logged)
        {
            $issuedAt = time();
            $expirationTime = $issuedAt + 6000; // jwt valid for 60 seconds from the issued time
            $payload = array(
            // id a stocker lors de l'utilisation de la bdd
            'login' => $searchLogin,
            'iat' => $issuedAt,
            'exp' => $expirationTime
            );
            $token_jwt = JWT::encode($payload,JWT_SECRET, "HS256");
            
            $response = $response->withHeader("Authorization", "Bearer {$token_jwt}")->withHeader("Content-Type", "application/json");
            $data = array('Login' => 'Connexion reussi', "Authorization" => "Bearer {$token_jwt}", "id" => $user->getId());

            return $response->withHeader("Content-Type", "application/json")->withJson($data);
        }
        else
        {
            $data = array('Erreur' => 'La combinaison Login / Mot de passe est incorrect');
            return $response->withJson($data);
        }
    });

$app->get('/api/users/{id}', function (Request $request, Response $response, array $args){
    global $entityManager;

    $user = $entityManager->getRepository('Clients')->find($args['id']);

    $res = array(
        'id' => $user->getId(),
        'nom' => $user->getNom(),
        'prenom' => $user->getPrenom(),
        'adresse' => $user->getAdresse(),
        'codePostal' => $user->getCodePostal(),
        'ville' => $user->getVille(),
        'telephone' => $user->getTelephone(),
        'civilite' => $user->getCivilite(),
        'email' => $user->getEmail(),
        'login' => $user->getLogin()
    );

    $obj = json_encode($res);
    $response->write($obj);
    return $response;
});

$app->post('/api/users/register', function (Request $request, Response $response, array $args) use ($logins) {
    $body = $request->getParsedBody(); // Parse le body
    
    global $entityManager;
    $entityManager->getConnection()->beginTransaction();
    try 
    {
        $client = new Clients;
        $client->setNom($body['nom']);
        $client->setPrenom($body['prenom']);
        $client->setAdresse($body['adresse']);
        $client->setCodePostal($body['codePostal']);
        $client->setVille($body['ville']);
        $client->setTelephone($body['telephone']);
        $client->setEmail($body['email']);
        $client->setCivilite($body['civilite']);
        $client->setLogin($body['login']);
        $client->setMdp(sha1($body['mdp']));

        $entityManager->persist($client);
        $entityManager->flush();
        $entityManager->getConnection()->commit();
    } 
    catch (Exception $e) 
    {
        $entityManager->getConnection()->rollBack();
        throw $e;
    }
    
    return json_encode($donnees);
});

$app->run();
