# Projet WEB : Angular / Doctrine

## Installation de la base de données

La base de données doit être installée en local. 
* Pour commencer, créer la base de données, puis executer le script d'installation des tables et des données qui se trouve à la racine du github : Tables.sql

* Se connecter à la base de données : `mysql -u root -p`
* Créer la base de données : `projetWeb`
* Importer les données du fichier Tables.sql : `use nom_base_de_donnees; source Tables.sql;`

* Utiliser une interface graphique comme Mysql Workbench pour se connecter et ajouter les données
* Connection avec les informations suivantes :
 Hostname : `localhost`
 Port : `3306`
 Username : `root`
 Password : `root`

## Installation du Back-End 

* Ouvrir un terminal et se déplacer dans le dossier `BACK`.
* Lancer la commande `php composer.phar install / php composer.phar update` pour installer les depandances.
* Lancer la commande `php -S localhost:8080` pour lancer le serveur.

## Installation du Front-End

* Ouvrir un terminal et se déplacer dans le dossier `FRONT`.
* Lancer la commande 'npm update' pour installer les dépendances qui se trouveront dans `node_modules`
* Lancer la commande `ng serve` pour lancer le serveur 
* (S'il y a besoin de modifier l'url de l'api il faut la modifier dans `src/environments/environments.ts`)

* L'application est disponible : `http://localhost:4200`
